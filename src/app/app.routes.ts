import { NgModule } from '@angular/core';

import { HomeComponent } from './home/home.component';
import { RadioComponent } from './radio/radio.component';

const routes: Routes = [
      { path: 'home', component: HomeComponent},
	  { path: 'radio', component: RadioComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'home' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule {}